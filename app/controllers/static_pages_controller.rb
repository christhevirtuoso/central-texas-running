class StaticPagesController < ApplicationController
  def home
  end

  def blog
  end

  def runs
  end

  def gear
  end
end
