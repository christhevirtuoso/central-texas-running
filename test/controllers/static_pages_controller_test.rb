require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get blog" do
    get :blog
    assert_response :success
  end

  test "should get runs" do
    get :runs
    assert_response :success
  end

  test "should get gear" do
    get :gear
    assert_response :success
  end

end
